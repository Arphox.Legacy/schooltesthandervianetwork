﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace ArphoxClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        MyClient client;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            client = new MyClient(this);
        }

        private void buttonSendFile_Click(object sender, RoutedEventArgs e)
        {
            #region Hibaellenőrzés
            if (textBoxServerIP.Text == string.Empty)
            {
                MessageBox.Show("Nem adtad meg a szerver IP-címét!"); return;
            }
            if (RosszNev(textBoxSajatNev.Text))
            {
                MessageBox.Show("Nem adtad meg a neved vagy rossz a megadott név!"); return;
            }
            if (textBoxValasztottFajl.Text == string.Empty)
            {
                MessageBox.Show("Nem adtál meg fájlt!"); return;
            }
            if (!File.Exists(textBoxValasztottFajl.Text))
            {
                MessageBox.Show("Nem létezik a megadott fájl!"); return;
            }
            #endregion

            try
            {
                client.SendFile(textBoxServerIP.Text, textBoxValasztottFajl.Text);
                labelBekuldve.Content = "Beküldve!";
                buttonSendFile.IsEnabled = false;
            }
            catch (Exception errr)
            {
                MessageBox.Show("Hiba!\n" + errr.Message);
            }
        }
        private bool RosszNev(string nev)
        {
            for (int i = 0; i < nev.Length; i++)
            {
                if (!(char.IsLetter(nev[i]) || char.IsWhiteSpace(nev[i])))
                {
                    return true;
                }
            }
            return false;
        }

        private void buttonTalloz_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ablak = new OpenFileDialog();
            ablak.Filter = "C# forráskódok (*.cs)|*.cs";
            ablak.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (ablak.ShowDialog() == true)
            {
                textBoxValasztottFajl.Text = ablak.FileName;
            }
        }
    }
    class MyClient
    {
        static int targetPort = 13000;
        MainWindow mainWindow;

        public MyClient(MainWindow window)
        {
            mainWindow = window;
        }
        public void SendFile(string server, string path)
        {
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(server), targetPort);

            // Create a TCP socket.
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint.
            client.Connect(ipEndPoint);

            string originalFileName = path;

            //Név és IP belerakása a fájl első sorába:
            string elsosor = string.Format("//[ARPHOXCLIENT] SENT BY: *{0}*\n//AT: {1}\n//FROM IP(s): *{2}*", mainWindow.textBoxSajatNev.Text, DateTime.Now.ToString(), GetLocaLIPAdresses());
            List<string> fajltartalma = File.ReadAllLines(originalFileName).ToList();
            fajltartalma.Insert(0, elsosor);

            string tempFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\157658243212387.txt"; //random temp fájlba írom
            File.WriteAllLines(tempFilePath, fajltartalma.ToArray());

            client.SendFile(tempFilePath);
            File.Delete(tempFilePath);

            client.Shutdown(SocketShutdown.Both); // Release the socket.
            client.Close();
        }
        public static string GetLocaLIPAdresses()
        {
            IPAddress[] coll = Dns.GetHostAddresses(Dns.GetHostName());
            string ip = string.Empty;
            int ennyiLesz = coll.Count(x => x.AddressFamily == AddressFamily.InterNetwork);

            if (ennyiLesz == 1)
            {
                return coll.Where(x => x.AddressFamily == AddressFamily.InterNetwork).First().ToString();
            }
            else
            {
                foreach (IPAddress x in coll)
                {
                    string xstring = x.ToString();
                    if (x.AddressFamily == AddressFamily.InterNetwork
                        && //és privát cím (B osztályosnál nem tökéletes de idc)
                        (xstring.StartsWith("10.") || xstring.StartsWith("172.") || xstring.StartsWith("192.168."))
                        ) //IPv4
                    {
                        ip += x.ToString() + " ";
                    }
                }
                return ip;
            }
        }

        #region older shit
        /*
        public void SendTextMessage(string server, string message)
        {
            try
            {
                TcpClient client = new TcpClient(server, targetPort);

                byte[] data = System.Text.Encoding.UTF8.GetBytes(message);

                NetworkStream stream = client.GetStream(); // Get a client stream for reading and writing.

                stream.Write(data, 0, data.Length); // Send the message to the connected TcpServer.

                mainWindow.listBox.Items.Add(string.Format("Elküldve: {0}", message));

                // Buffer to store the response bytes.
                data = new byte[256];

                // String to store the response ASCII representation.
                string responseData = string.Empty;

                // Read the first batch of the TcpServer response bytes.
                int bytes = stream.Read(data, 0, data.Length);
                //responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                responseData = System.Text.Encoding.UTF8.GetString(data, 0, bytes);
                mainWindow.listBox.Items.Add(string.Format("Fogadva: {0}", responseData));

                // Close everything.
                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException e)
            {
                mainWindow.listBox.Items.Add(string.Format("ArgumentNullException: {0}", e));
            }
            catch (SocketException e)
            {
                mainWindow.listBox.Items.Add(string.Format("SocketException: {0}", e));
            }

            //mainWindow.listBox.Items.Add(string.Format("\n Press Enter to continue..."));
            //Console.Read();
        }
        */
        #endregion
    }
}